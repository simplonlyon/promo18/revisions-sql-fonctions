# revisions-sql-fonctions

 - À partir de la base de Damien, écrivez la requête permettant d'afficher le prix moyen des produits pour Femme :  
 `SELECT ROUND(AVG(price),2) AS PRIX_MOYEN FROM product WHERE gender = 'Femme';`  
 Aide : commencer par afficher le prix de tous les produits pour femme avec `SELECT price FROM product WHERE gender = 'Femme'`, puis rajouter ensuite les fonctions et l'alias jusqu'à obtenir le bon résultat
 - Affichez le prix moyen des produits par couleur  
 `SELECT color, ROUND(AVG(price),2) AS PRIX_MOYEN FROM product GROUP BY color;`
 - Affichez le Chiffre d'Affaires du site au mois de mai 2022  
 ```sql
 SELECT ROUND(SUM(i.price * i.quantity),2) AS CA_MAI_2022  
    FROM order_table o 
        INNER JOIN item i
            ON i.order_id = o.id
    WHERE YEAR(o.date) = 2022 
        AND MONTH(o.date) = 05;
```

Autre possibilité : 
```sql
SELECT ROUND(SUM(i.price * i.quantity),2) AS CA_MAI_2022  
    FROM order_table o 
        INNER JOIN item i
            ON i.order_id = o.id
    WHERE o.date LIKE '2022-05%';
```
En fait, MySQL permet de traiter les dates comme des chaînes de caractères, donc on peut tricher !
 - Affichez le CA du site par mois
```sql
SELECT 
    MONTH(o.date) AS 'Mois',
    YEAR(o.date) AS 'Année', 
    OUND(SUM(i.price*i.quantity),2) AS 'CA' 
    FROM order_table o 
        RIGHT JOIN item i
            ON i.order_id=o.id
    GROUP BY Année, Mois;
```
